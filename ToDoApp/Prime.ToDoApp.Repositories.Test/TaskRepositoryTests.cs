﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp.Data;
using Prime.ToDoApp.Data.Mapper;
using Prime.ToDoApp.Data.Repositories;
using Prime.ToDoApp.Data.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Prime.ToDoApp.Repositories.Test
{
    public class TaskRepositoryTests : IDisposable
    {
        private readonly DbContextOptions<ToDoAppDbContext> _dbContextOptions;
        private readonly IMapper _mapper;
        private readonly TaskRepository _repository;
        private readonly ToDoAppDbContext _context;

        public TaskRepositoryTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ToDoAppDbContext>()
                .UseInMemoryDatabase(databaseName: "ToDoAppDBBB")
                .Options;

            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new MapperProfile());
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
            }

            _context = new ToDoAppDbContext(_dbContextOptions);
            _repository = new TaskRepository(_context, _mapper);
        }

        public async void Dispose()
        {
            await _context.Database.EnsureDeletedAsync();
        }

        public async Task SeedDataAsync(ToDoAppDbContext context)
        {
            await Seed.SeedTasksAsync(context);
            await Seed.SeedProjectsAsync(context);
        }

        [Fact]
        public async Task CheckIfCreated()
        {
            var newTask = new Data.Entities.Task();
            newTask.Id = 1;
            newTask.Name = "Sth";
            newTask.Description = "desc";
            newTask.IsDone = true;
            newTask.ProjectId = 1;

            await _repository.CreateAsync(newTask);

            Assert.Equal(1, await _context.Tasks.CountAsync());
        }

        [Fact]
        public async Task CheckRead()
        {
            var newTask = new Data.Entities.Task();
            newTask.Id = 1;
            newTask.Name = "Sth";
            newTask.Description = "desc";
            newTask.IsDone = true;
            newTask.ProjectId = 1;

            await _repository.CreateAsync(newTask);

            var task = await _repository.GetByIdAsync<Data.Entities.Task>(1);
            var name = task.Name;
            var description = task.Description;
            var isDone = task.IsDone;
            var projectId = task.ProjectId;

            Assert.Equal("Sth", name);
            Assert.Equal("desc", description);
            Assert.True(isDone);
            Assert.Equal(1, projectId);
        }

        [Fact]
        public async Task CheckUpdate()
        {
            var newTask = new Data.Entities.Task();
            newTask.Id = 1;
            newTask.Name = "Sth";
            newTask.Description = "desc";
            newTask.IsDone = true;
            newTask.ProjectId = 1;

            await _repository.CreateAsync(newTask);
            newTask.Name = "name changed";

            await _repository.UpdateAsync(newTask);

            var count = await _context.Tasks.CountAsync(x => x.Name == "name changed");

            Assert.Equal(1, count);
        }

        [Fact]
        public async Task CheckIfDeleted()
        {
            var newTask = new Data.Entities.Task()
            {
                Id = 1,
                Name = "Sth",
                Description = "desc",
                IsDone = true,
                ProjectId = 1
            };

            var entity = await _repository.CreateAsync(newTask);

            await _repository.DeleteAsync(entity.Id);

            Assert.True(await _context.Tasks.CountAsync() == 0);
        }

        [Fact]
        public async Task CheckReadAll()
        {
            await SeedDataAsync(_context);

            var count = _repository.GetAllAsync<Data.Entities.Task>(1, 4).Result.TotalCount;

            Assert.Equal(3, count);
        }
    }
}
