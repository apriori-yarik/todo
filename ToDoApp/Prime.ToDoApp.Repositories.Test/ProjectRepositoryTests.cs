﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp.Data;
using Prime.ToDoApp.Data.Mapper;
using Prime.ToDoApp.Data.Repositories;
using Prime.ToDoApp.Data.Seeders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Prime.ToDoApp.Repositories.Test
{
    public class ProjectRepositoryTests : IDisposable
    {
        private readonly DbContextOptions<ToDoAppDbContext> _dbContextOptions;
        private readonly IMapper _mapper;
        private readonly ToDoAppDbContext _context;
        private readonly ProjectRepository _repository;

        public async void Dispose()
        {
            await _context.Database.EnsureDeletedAsync();
        }

        public ProjectRepositoryTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ToDoAppDbContext>()
                .UseInMemoryDatabase(databaseName: "ToDoAppDBBBB")
                .Options;

            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new MapperProfile());
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
            }

            _context = new ToDoAppDbContext(_dbContextOptions);
            _repository = new ProjectRepository(_context, _mapper);
        }

        public async Task SeedDataAsync(ToDoAppDbContext context)
        {
            await Seed.SeedTasksAsync(context);
            await Seed.SeedProjectsAsync(context);
        }

        [Fact]
        public async Task CanReadProject()
        {
            await SeedDataAsync(_context);

            var project = await _repository.GetByIdAsync<Data.Entities.Project>(2);

            Assert.Equal("some-project2", project.Name);
        }

        [Fact]
        public async Task CanReadAllProjects()
        {
            await SeedDataAsync(_context);

            var count = _repository.GetAllAsync<Data.Entities.Project>(1, 4).Result.TotalCount;

            Assert.Equal(2, count);
        }

        [Fact]
        public async Task CanCreateProject()
        {
            var newProject = new Data.Entities.Project();
            newProject.Name = "new project";

            await _repository.CreateAsync(newProject);

            var count = _context.Projects.Count();
            Assert.Equal(1, count);
        }

        [Fact]
        public async Task CanUpdateProject()
        { 
            await SeedDataAsync(_context);

            var project = await _context.Projects.SingleOrDefaultAsync(x => x.Id == 1);
            
            project.Name = "name changed";
            _context.Entry(project).State = EntityState.Detached;

            await _repository.UpdateAsync(project);

            var count = await _context.Projects.CountAsync(x => x.Name == "name changed");
            Assert.Equal(1, count);
        }

        [Fact]
        public async Task CanDeleteProject()
        {
            var project = new Data.Entities.Project();
            project.Name = "sth";

            await _repository.CreateAsync(project);

            await _repository.DeleteAsync(1);

            Assert.Equal(0, _context.Projects.Count());
        }
    }
}
