﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prime.ToDoApp.Domain.Dtos
{
    public class ProjectDtoWithId
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public ICollection<TaskDtoWithId> Tasks { get; set; }
    }
}
