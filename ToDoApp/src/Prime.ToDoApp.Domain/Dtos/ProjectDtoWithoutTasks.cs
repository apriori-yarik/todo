﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Domain.Dtos
{
    public class ProjectDtoWithoutTasks
    {
        public string Name { get; set; }
    }
}
