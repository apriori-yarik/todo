﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Domain.Dtos
{
    public class ProjectDtoWithIdWithoutTasks
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
