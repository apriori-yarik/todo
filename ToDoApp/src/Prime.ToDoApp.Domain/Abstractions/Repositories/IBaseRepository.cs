﻿using Prime.ToDoApp.Domain.Dtos;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Domain.Abstractions.Repositories
{
    public interface IBaseRepository
    {
        Task<TDto> GetByIdAsync<TDto>(long id);
        Task<PaginatedResult<TDto>> GetAllAsync<TDto>(
            int pageNumber, int pageSize, Expression<Func<TDto, bool>> filter = null);
        Task<TDto> CreateAsync<TDto>(TDto dto);
        Task UpdateAsync<TDto>(TDto dto);
        Task DeleteAsync(long id);
    }
}
