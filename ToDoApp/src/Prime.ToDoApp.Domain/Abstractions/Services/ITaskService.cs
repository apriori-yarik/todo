﻿using Prime.ToDoApp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Domain.Abstractions.Services
{
    public interface ITaskService
    {
        Task<TaskDtoWithId> GetAsync(long id);

        Task<PaginatedResult<TaskDtoWithId>> GetAllAsync(int page, int pageSize);

        Task CreateAsync(TaskDto taskDto);

        Task DeleteAsync(long id);

        Task UpdateAsync(TaskDtoWithId taskDtoWithId);
    }
}
