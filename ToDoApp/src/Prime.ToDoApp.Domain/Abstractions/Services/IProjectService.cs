﻿using Prime.ToDoApp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Domain.Abstractions.Services
{
    public interface IProjectService
    {
        Task<ProjectDtoWithId> GetAsync(long id);

        Task<PaginatedResult<ProjectDtoWithId>> GetAllAsync(int page, int pageSize);

        Task CreateAsync(ProjectDtoWithoutTasks projectDto);

        Task DeleteAsync(long id);

        Task UpdateAsync(ProjectDtoWithIdWithoutTasks projectDtoWithId);
    }
}
