﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Prime.ToDoApp.Domain.Abstractions.Services;
using Prime.ToDoApp.Domain.Dtos;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDtoWithId>> GetProjectAsync(long id)
        {
            var project = await _projectService.GetAsync(id);

            if (project == null)
            {
                return NotFound();
            }

            return Ok(project);
        }

        [HttpGet]
        public async Task<ActionResult<PaginatedResult<ProjectDtoWithId>>> GetPaginatedResultAsync(
            [FromQuery] int page, [FromQuery] int pageSize)
        {
            var projects = await _projectService.GetAllAsync(page, pageSize);

            if (projects.TotalCount == 0)
            {
                return NotFound();
            }

            return Ok(projects);
        }

        [HttpPost]
        public async Task<ActionResult> CreateProjectAsync(ProjectDtoWithoutTasks dto)
        {
            await _projectService.CreateAsync(dto);

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateProjectAsync(long id, ProjectDtoWithIdWithoutTasks dto)
        {
            dto.Id = id;
            await _projectService.UpdateAsync(dto);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteProjectAsync(long id)
        {
            await _projectService.DeleteAsync(id);

            return NoContent();
        }
    }
}
