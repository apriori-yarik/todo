﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Prime.ToDoApp.Data.Repositories;
using Prime.ToDoApp.Domain.Abstractions.Services;
using Prime.ToDoApp.Domain.Dtos;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TaskController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDtoWithId>> GetTaskAsync(long id)
        {
            var task = await _taskService.GetAsync(id);

            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        [HttpGet]
        public async Task<ActionResult<PaginatedResult<TaskDtoWithId>>> GetTasksPaginatedAsync(
            [FromQuery] int page, [FromQuery] int pageSize)
        {
            var tasks = await _taskService.GetAllAsync(page, pageSize);

            if (tasks.TotalCount == 0)
            {
                return NotFound();
            }

            return Ok(tasks);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTaskAsync(TaskDto task)
        {
            await _taskService.CreateAsync(task);

            return NoContent();
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateTaskAsync(long id, TaskDtoWithId taskDtoWithId)
        {
            taskDtoWithId.Id = id;
            await _taskService.UpdateAsync(taskDtoWithId);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTaskAsync(long id)
        {
            await _taskService.DeleteAsync(id);

            return NoContent();
        }
    }
}
