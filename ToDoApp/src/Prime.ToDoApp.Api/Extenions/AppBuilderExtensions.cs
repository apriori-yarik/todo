﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Prime.ToDoApp.Data;
using Prime.ToDoApp.Data.Seeders;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Api.Extenions
{
    public static class AppBuilderExtensions
    {
        public static async Task UpdateDatabaseAsync(this IApplicationBuilder app)
        {
            using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
            var context = serviceScope.ServiceProvider.GetRequiredService<ToDoAppDbContext>();
            await context.Database.MigrateAsync();
            await SeedDataAsync(context);
        }

        private static async Task SeedDataAsync(ToDoAppDbContext context)
        {
            await Seed.SeedProjectsAsync(context);
            await Seed.SeedTasksAsync(context);
        }
    }
}
