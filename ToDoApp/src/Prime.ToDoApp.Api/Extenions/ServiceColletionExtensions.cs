﻿using AutoMapper.Extensions.ExpressionMapping;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Prime.ToDoApp.Data;

namespace Prime.ToDoApp.Api.Extenions
{
    public static class ServiceColletionExtensions
    {
        public static IServiceCollection AddToDoAppDbContext(
            this IServiceCollection services, string connString) 
            => services.AddDbContext<ToDoAppDbContext>(
                options => options.UseSqlServer(connString));

        public static IServiceCollection AddToDoAppAutomapper(this IServiceCollection services) 
            => services.AddAutoMapper(mc =>
                {
                    mc.AddExpressionMapping();
                    mc.AddProfile(new Mapper.MapperProfile());
                    mc.AddProfile(new Data.Mapper.MapperProfile());
                });

        public static IServiceCollection AddToDoAppSwagger(this IServiceCollection services)
            => services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "ToDoApp API", Version = "v1" });
            });
    }
}
