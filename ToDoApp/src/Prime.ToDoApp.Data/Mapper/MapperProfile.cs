﻿using AutoMapper;
using Prime.ToDoApp.Data.Entities;
using Prime.ToDoApp.Domain.Dtos;

namespace Prime.ToDoApp.Data.Mapper
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Task, Task>();
            CreateMap<Project, Project>();

            CreateMap<TaskDto, Task>().ReverseMap();
            CreateMap<TaskDtoWithId, Task>().ReverseMap();

            CreateMap<ProjectDto, Project>().ReverseMap();
            CreateMap<ProjectDtoWithId, Project>().ReverseMap();
            CreateMap<ProjectDtoWithoutTasks, Project>().ReverseMap();
            CreateMap<ProjectDtoWithIdWithoutTasks, Project>().ReverseMap();
        }
    }
}
