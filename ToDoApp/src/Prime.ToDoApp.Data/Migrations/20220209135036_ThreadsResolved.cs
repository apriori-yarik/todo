﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Prime.ToDoApp.Data.Migrations
{
    public partial class ThreadsResolved : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Done",
                table: "Tasks",
                newName: "IsDone");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "IsDone",
                table: "Tasks",
                newName: "Done");
        }
    }
}
