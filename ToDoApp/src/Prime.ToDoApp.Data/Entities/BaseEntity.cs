﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Prime.ToDoApp.Data.Entities
{
    public class BaseEntity
    {
        [Key]
        public long Id { get; set; }

        [Required]
        [MinLength(3)]
        public string Name { get; set; }
    }
}
