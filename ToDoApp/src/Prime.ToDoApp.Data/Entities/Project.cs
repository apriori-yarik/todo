﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Data.Entities
{
    public class Project : BaseEntity
    {
        public ICollection<Task> Tasks { get; set; }
    }
}
