﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Data.Entities
{
    public class Task : BaseEntity
    {
        public string Description { get; set; }

        public bool IsDone { get; set; }

        public long ProjectId { get; set; }

        public Project Project { get; set; }
    }
}
