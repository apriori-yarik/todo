﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp;
using Prime.ToDoApp.Data;

namespace Prime.ToDoApp.Data.Seeders
{
    public class Seed
    {
        public static async Task SeedProjectsAsync(ToDoAppDbContext context)
        {
            if (await context.Projects.AnyAsync()) return;

            var projects = new List<Entities.Project>
            {
                new Entities.Project
                {
                    Name = "some-project"
                },
                new Entities.Project
                {
                    Name = "some-project2"
                }
            };

            await context.Projects.AddRangeAsync(projects);
            await context.SaveChangesAsync();
        }

        public static async Task SeedTasksAsync(ToDoAppDbContext context)
        {
            if (await context.Tasks.AnyAsync()) return;

            var tasks = new List<Entities.Task>
            {
                new Entities.Task
                {
                    Name = "task1",
                    Description = "desc1",
                    IsDone = true,
                    ProjectId = 2
                },
                new Entities.Task
                {
                    Name = "task2",
                    Description = "desc2",
                    IsDone = false,
                    ProjectId = 2
                },
                new Entities.Task
                {
                    Name = "task3",
                    Description = "desc3",
                    IsDone = true,
                    ProjectId = 1
                }
            };

            await context.Tasks.AddRangeAsync(tasks);
            await context.SaveChangesAsync();
        }
    }
}
