﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp.Data.Entities;
using Prime.ToDoApp.Data.Extensions;
using Prime.ToDoApp.Domain.Abstractions.Repositories;
using Prime.ToDoApp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Data.Repositories
{
    public class ProjectRepository : BaseRepository<Project>, IProjectRepository
    {
        public ProjectRepository(ToDoAppDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }

        public override async Task<TDto> GetByIdAsync<TDto>(long id)
        {
            var items = await Items.Include(x => x.Tasks).FirstOrDefaultAsync(x => x.Id == id);
            return Mapper.Map<TDto>(items);
        }

        protected override IQueryable<Project> OnBeforeGetAll()
        {
            return Items.Include(x => x.Tasks);
        }
    }
}
