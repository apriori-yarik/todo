﻿using AutoMapper;
using Prime.ToDoApp.Data.Entities;
using Prime.ToDoApp.Domain.Abstractions.Repositories;

namespace Prime.ToDoApp.Data.Repositories
{
    public class DummyRepository : BaseRepository<BaseEntity>, IDummyRepository
    {
        public DummyRepository(ToDoAppDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {
        }
    }
}
