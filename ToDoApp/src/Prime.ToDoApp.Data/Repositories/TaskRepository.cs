﻿using AutoMapper;
using Prime.ToDoApp.Data.Entities;
using Prime.ToDoApp.Domain.Abstractions.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Prime.ToDoApp.Data.Repositories
{
    public class TaskRepository : BaseRepository<Task>, ITaskRepository
    {
        public TaskRepository(ToDoAppDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
        {

        }
    }
}
