﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp.Data.Extensions;
using Prime.ToDoApp.Domain.Abstractions.Repositories;
using Prime.ToDoApp.Domain.Dtos;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Data.Repositories
{
    public class BaseRepository<TEntity> : IBaseRepository
        where TEntity: Entities.BaseEntity
    {
        protected IMapper Mapper { get; }
        protected ToDoAppDbContext DbContext { get; }
        protected DbSet<TEntity> Items { get; }

        public BaseRepository(ToDoAppDbContext dbContext, IMapper mapper)
        {
            DbContext = dbContext;
            Items = DbContext.Set<TEntity>();
            Mapper = mapper;
        }
        protected virtual IQueryable<TEntity> OnBeforeGetAll()
        {
            return Items;
        }

        public async virtual Task<TDto> GetByIdAsync<TDto>(long id) 
            => Mapper.Map<TDto>(await OnBeforeGetAll().FirstOrDefaultAsync(x => x.Id == id));

        public async virtual Task<PaginatedResult<TDto>> GetAllAsync<TDto>(int pageNumber, int pageSize, Expression<Func<TDto, bool>> filter = null)
        {
            var query = OnBeforeGetAll().AsQueryable();

            if (filter != null)
                query = query.Where(Mapper.Map<Expression<Func<TEntity, bool>>>(filter));

            return await Mapper.ProjectTo<TDto>(query).PaginateAsync(pageNumber, pageSize);
        }

        public async virtual Task<TDto> CreateAsync<TDto>(TDto dto)
        {
            var entity = Mapper.Map<TEntity>(dto);

            await Items.AddAsync(entity);
            await DbContext.SaveChangesAsync();

            DbContext.Entry(entity).State = EntityState.Detached;

            return Mapper.Map<TDto>(entity);
        }

        public async virtual Task UpdateAsync<TDto>(TDto dto)
        {
            var entity = Mapper.Map<TEntity>(dto);

            DbContext.Set<TEntity>().Update(entity);
            await DbContext.SaveChangesAsync();
        }

        public async virtual Task DeleteAsync(long id)
        {
            var entity = await Items
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == id);

            if (entity != null)
            {
                DbContext.Set<TEntity>().Remove(entity);
                await DbContext.SaveChangesAsync();
            }
        }
    }
}
