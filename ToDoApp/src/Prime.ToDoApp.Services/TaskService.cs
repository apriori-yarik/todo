﻿using Prime.ToDoApp.Domain.Abstractions.Repositories;
using Prime.ToDoApp.Domain.Abstractions.Services;
using Prime.ToDoApp.Domain.Dtos;
using Prime.ToDoApp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Services
{
    public class TaskService : ITaskService
    {
        private readonly ITaskRepository _taskRepository;

        public TaskService(ITaskRepository taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public async Task<TaskDtoWithId> GetAsync(long id)
        {
            var task = await _taskRepository.GetByIdAsync<TaskDtoWithId>(id);

            return task;
        }

        public async Task<PaginatedResult<TaskDtoWithId>> GetAllAsync(int page, int pageSize)
        {
            var tasks = await _taskRepository.GetAllAsync<TaskDtoWithId>(page, pageSize);

            return tasks;
        }

        public async Task CreateAsync(TaskDto taskDto)
        {
            await _taskRepository.CreateAsync(taskDto);
        }

        public async Task DeleteAsync(long id)
        {
            await _taskRepository.DeleteAsync(id);
        }

        public async Task UpdateAsync(TaskDtoWithId taskDtoWithId)
        {
            await _taskRepository.UpdateAsync(taskDtoWithId);
        } 
        
    }
}
