﻿using Prime.ToDoApp.Domain.Abstractions.Repositories;
using Prime.ToDoApp.Domain.Abstractions.Services;
using Prime.ToDoApp.Domain.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prime.ToDoApp.Services
{
    public class ProjectService : IProjectService
    {
        private readonly IProjectRepository _projectRepository;

        public ProjectService(IProjectRepository projectRepository)
        {
            _projectRepository = projectRepository;
        }

        public async Task CreateAsync(ProjectDtoWithoutTasks projectDto)
        {
            await _projectRepository.CreateAsync(projectDto);
        }

        public async Task DeleteAsync(long id)
        {
            await _projectRepository.DeleteAsync(id);
        }

        public async Task<PaginatedResult<ProjectDtoWithId>> GetAllAsync(int page, int pageSize)
        {
            return await _projectRepository.GetAllAsync<ProjectDtoWithId>(page, pageSize);
        }

        public async Task<ProjectDtoWithId> GetAsync(long id)
        {
            return await _projectRepository.GetByIdAsync<ProjectDtoWithId>(id);
        }

        public async Task UpdateAsync(ProjectDtoWithIdWithoutTasks projectDtoWithId)
        {
            
            await _projectRepository.UpdateAsync(projectDtoWithId);
        }
    }
}
