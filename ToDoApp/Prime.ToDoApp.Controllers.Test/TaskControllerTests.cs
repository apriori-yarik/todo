﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp.Api.Controllers;
using Prime.ToDoApp.Data;
using Prime.ToDoApp.Data.Mapper;
using Prime.ToDoApp.Data.Repositories;
using Prime.ToDoApp.Data.Seeders;
using Prime.ToDoApp.Domain.Abstractions.Repositories;
using Prime.ToDoApp.Domain.Abstractions.Services;
using Prime.ToDoApp.Domain.Dtos;
using Prime.ToDoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Prime.ToDoApp.Controllers.Test
{
    public class TaskControllerTests : IDisposable
    {
        private readonly DbContextOptions<ToDoAppDbContext> _dbContextOptions;
        private readonly IMapper _mapper;
        private readonly ITaskRepository _repository;
        private readonly ToDoAppDbContext _context;
        private readonly ITaskService _service;
        private readonly TaskController _controller;

        public TaskControllerTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ToDoAppDbContext>()
                .UseInMemoryDatabase(databaseName: "ToDoAppDBControllers")
                .Options;

            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new MapperProfile());
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
            }

            _context = new ToDoAppDbContext(_dbContextOptions);
            _repository = new TaskRepository(_context, _mapper);
            _service = new TaskService(_repository);
            _controller = new TaskController(_service);
        }

        public async void Dispose()
        {
            await _context.Database.EnsureDeletedAsync();
        }

        public async Task SeedDataAsync(ToDoAppDbContext context)
        {
            await Seed.SeedTasksAsync(context);
            await Seed.SeedProjectsAsync(context);
        }

        [InlineData(1)]
        [Theory]
        public async Task CheckGetAsyncWhenTaskExist(long id)
        {
            await SeedDataAsync(_context);

            var action = await _controller.GetTaskAsync(id);

            Assert.IsType<OkObjectResult>(action.Result);

            var result = action.Result as OkObjectResult;

            Assert.IsType<TaskDtoWithId>(result.Value);

            var task = result.Value as TaskDtoWithId;

            Assert.Equal("task1", task.Name);
        }

        [InlineData(10)]
        [Theory]
        public async Task CheckGetAsyncWhenTaskDoesNotExist(long id)
        {
            await SeedDataAsync(_context);

            var action = await _controller.GetTaskAsync(id);

            Assert.IsType<NotFoundResult>(action.Result);
        }

        [Fact]
        public async Task CheckCreateAsync()
        {
            var newTask = new TaskDto()
            {
                Name = "new-task1002",
                Description = "desc100",
                IsDone = true,
                ProjectId = 3
            };

            var action = await _controller.CreateTaskAsync(newTask);

            Assert.IsType<NoContentResult>(action);
        }

        [Fact]
        public async Task CheckReadAllAsync()
        {
            await SeedDataAsync(_context);

            var action = await _controller.GetTasksPaginatedAsync(1, 2);

            Assert.IsType<OkObjectResult>(action.Result);

            var result = action.Result as OkObjectResult;

            var tasks = result.Value as PaginatedResult<TaskDtoWithId>;

            Assert.IsType<PaginatedResult<TaskDtoWithId>>(tasks);

            Assert.Equal(3, tasks.TotalCount);
        }

        [Fact]
        public async Task CheckDeleteAsync()
        {
            var newTask = new Data.Entities.Task()
            {
                Id = 1,
                Name = "Sth",
                Description = "desc",
                IsDone = true,
                ProjectId = 1
            };

            var entity = await _repository.CreateAsync(newTask);

            Assert.Equal(1, _context.Tasks.Count());

            var action = await _controller.DeleteTaskAsync(entity.Id);

            Assert.IsType<NoContentResult>(action);

            Assert.Equal(0, _context.Tasks.Count());
        }

        [Fact]
        public async Task CheckUpdateAsync()
        {
            await SeedDataAsync(new ToDoAppDbContext(_dbContextOptions));

            var task = new TaskDtoWithId();
            task.Name = "name changed";
            task.Desciption = "desc1";
            task.ProjectId = 2;
            task.IsDone = true;

            var action = await _controller.UpdateTaskAsync(1, task);

            Assert.IsType<NoContentResult>(action);
            Assert.Equal(1, _context.Tasks.Count(x => x.Name == "name changed"));
        }
    }
}
