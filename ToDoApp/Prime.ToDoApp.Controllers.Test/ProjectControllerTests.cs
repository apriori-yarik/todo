﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Prime.ToDoApp.Api.Controllers;
using Prime.ToDoApp.Data;
using Prime.ToDoApp.Data.Mapper;
using Prime.ToDoApp.Data.Repositories;
using Prime.ToDoApp.Data.Seeders;
using Prime.ToDoApp.Domain.Abstractions.Repositories;
using Prime.ToDoApp.Domain.Abstractions.Services;
using Prime.ToDoApp.Domain.Dtos;
using Prime.ToDoApp.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Prime.ToDoApp.Controllers.Test
{
    public class ProjectControllerTests : IDisposable
    {
        private readonly DbContextOptions<ToDoAppDbContext> _dbContextOptions;
        private readonly IMapper _mapper;
        private readonly IProjectRepository _repository;
        private readonly ToDoAppDbContext _context;
        private readonly IProjectService _service;
        private readonly ProjectController _controller;

        public ProjectControllerTests()
        {
            _dbContextOptions = new DbContextOptionsBuilder<ToDoAppDbContext>()
                .UseInMemoryDatabase(databaseName: "ToDoAppDBProjectController")
                .Options;

            if (_mapper == null)
            {
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new MapperProfile());
                });
                IMapper mapper = mappingConfig.CreateMapper();
                _mapper = mapper;
            }

            _context = new ToDoAppDbContext(_dbContextOptions);
            _repository = new ProjectRepository(_context, _mapper);
            _service = new ProjectService(_repository);
            _controller = new ProjectController(_service);
        }

        public async void Dispose()
        {
            await _context.Database.EnsureDeletedAsync();
        }

        public async Task SeedDataAsync(ToDoAppDbContext context)
        {
            await Seed.SeedTasksAsync(context);
            await Seed.SeedProjectsAsync(context);
        }

        [InlineData(1)]
        [Theory]
        public async Task CheckGetAsyncWhenProjectExist(long id)
        {
            await SeedDataAsync(_context);

            var action = await _controller.GetProjectAsync(id);

            Assert.IsType<OkObjectResult>(action.Result);

            var result = action.Result as OkObjectResult;

            Assert.IsType<ProjectDtoWithId>(result.Value);

            var task = result.Value as ProjectDtoWithId;

            Assert.Equal("some-project", task.Name);
        }

        [InlineData(10)]
        [Theory]
        public async Task CheckGetAsyncWhenProjectDoesNotExist(long id)
        {
            await SeedDataAsync(_context);

            var action = await _controller.GetProjectAsync(id);

            Assert.IsType<NotFoundResult>(action.Result);
        }

        [Fact]
        public async Task CheckReadAllAsync()
        {
            await SeedDataAsync(_context);

            var action = await _controller.GetPaginatedResultAsync(1, 2);

            Assert.IsType<OkObjectResult>(action.Result);

            var result = action.Result as OkObjectResult;

            var tasks = result.Value as PaginatedResult<ProjectDtoWithId>;

            Assert.IsType<PaginatedResult<ProjectDtoWithId>>(tasks);

            Assert.Equal(2, tasks.TotalCount);
        }

        [Fact]
        public async Task CheckCreateAsync()
        {
            var newProject = new ProjectDtoWithoutTasks()
            {
                Name = "new-project",
            };

            var action = await _controller.CreateProjectAsync(newProject);

            Assert.IsType<NoContentResult>(action);
        }

        [Fact]
        public async Task CheckDeleteAsync()
        {
            var newProject = new ProjectDtoWithIdWithoutTasks()
            {
                Id = 1,
                Name = "Sth"
            };

            var entity = await _repository.CreateAsync(newProject);

            Assert.Equal(1, _context.Projects.Count());

            var action = await _controller.DeleteProjectAsync(entity.Id);

            Assert.IsType<NoContentResult>(action);

            Assert.Equal(0, _context.Projects.Count());
        }

        [Fact]
        public async Task CheckUpdateAsync()
        {
            await SeedDataAsync(new ToDoAppDbContext(_dbContextOptions));

            ProjectDtoWithIdWithoutTasks entity = new ProjectDtoWithIdWithoutTasks();
            entity.Name = "name changed";

            var action = await _controller.UpdateProjectAsync(1, entity);

            Assert.IsType<NoContentResult>(action);
            Assert.Equal(1, _context.Projects.Count(x => x.Name == "name changed"));
        }
    }
}
